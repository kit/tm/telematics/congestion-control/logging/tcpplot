#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Documentation, License etc.

@package PlotTcpProbe
'''

import os
import matplotlib
import matplotlib.pyplot as plt
from ipaddress import ip_address
from collections import defaultdict, OrderedDict

from TcpProbeSample import TcpProbeSample, TcpLogSample



## experiment specific manipulators


# data

#def get_grouping_value(sample):
    #return "%s:%s" % sample.src_port, sample.dst_port


def get_time_offset(sample):
    if ( str(sample.src_ip) in ("10.0.1.2", "10.0.2.2") ):
        return 60.0

    return 0.0



def set_y_limits_1g(ax_cwnd=None, ax_rtt=None):
    if ( ax_cwnd ):
        ax_cwnd.set_ylim(ymin=0)
        ax_cwnd.set_ylim(ymax=1400)

    if ( ax_rtt ):
        ax_rtt.set_ylim(ymin=0)
        #ax_rtt.set_ylim(ymax=16)
        ax_rtt.set_ylim(ymax=17)   ## XXX NeIF

def set_y_limits_10g(ax_cwnd=None, ax_rtt=None):
    if ( ax_cwnd ):
        ax_cwnd.set_ylim(ymin=0)
        ax_cwnd.set_ylim(ymax=18000)

    if ( ax_rtt ):
        ax_rtt.set_ylim(ymin=0)
        #ax_rtt.set_ylim(ymax=16)
        ax_rtt.set_ylim(ymax=100)   ## XXX NeIF



def plot_min_rtt(ax_rtt, y_value=5):
    X = ax_rtt.get_xlim()
    Y = [ y_value, y_value ]

    ax_rtt.plot(X, Y, color="grey", linestyle="--")



# plot

def adjust_x_limits(ax):
    ax.set_xlim(0, 360)

def adjust_y1_limits(ax, values=None):
    ax.set_ylim(ymin=0)
    
    try:
        if ( values ):
            __max = max(values)
            __limit = __max + min(50, __max*0.1)
            ax.set_ylim(ymax=__limit)
    except:
        pass

def adjust_y2_limits(ax, values=None):
    adjust_y1_limits(ax, values)


## [experiment specific manipulators]



# files

def create_output_filename(input_filename, output_type, all_keys, key, output_dir=None):
    """
      Use this with '--multi-output'
    """

    file_and_ext = os.path.splitext(os.path.basename(input_filename))

    conn = ""
    if ( len(all_keys) > 1 ):
        conn = "_" + key

    if ( output_dir == None ):
        output_dir = ""
    else:
        os.makedirs(output_dir, exist_ok=True)
        output_dir += "/"

    return output_dir + file_and_ext[0] + conn + "." + args.output



def get_output_filename(args):
    """
      Use this with '--one-output'
    """

    ## output-filename is given explicitly
    if ( args.output_filename ):
        filename = args.output_filename
    ## otherwise, the original filename is used (with adapted filename extension)
    else:
        filename = args.files[0]


    ## if output_dir is given, strip the original path from the filename (if there is any)
    if ( args.output_dir ):
        os.makedirs(args.output_dir, exist_ok=True)
        out_filename = args.output_dir + "/" + os.path.basename(filename)
    else:
        out_filename = filename

    ## replace filename extension to the output format
    out_filename = os.path.splitext(out_filename)[0] + "." + args.output

    return out_filename




def read_tcp_log_header( file_object ):
    def __read():
        return file_object.readline().strip()

    header = OrderedDict()


    ## read header
    line = __read()
    assert line == "#TCPLOG"
    
    while not line.startswith("#END_HEADER"):
        split_line = line.split(" ", 1)
        if ( len(split_line) == 1 ):
            split_line.append("[Present]")
            
        header[ split_line[0] ] = split_line[1]
        
        line = __read()


    ## Print header
    for key in header:
        print( "{} {}".format(key, header[key]) )


    ## get required data    
    timestamp = float( header["#TIMESTAMP:"] )
    format = header["#FORMAT:"]

    return timestamp, format



def get_opener(filename):
    if filename.endswith(".bz2"):
        import bz2
        return bz2.open
        
    else:
        return open


def read_file(filename, sampling=None, file_type="tcplog"):
    connections = defaultdict(list)
    last_sample = defaultdict( lambda: float("-inf") )

    is_tcp_log = ( file_type == "tcplog" )

    ## automatically handle compressed files
    open_func = get_opener(filename)
    with open_func(filename, mode="tr", encoding="UTF-8") as f:

        if ( is_tcp_log ):
            timestamp, format = read_tcp_log_header( f )
            layout = format.split(' ')

        for line in f:
            ## skip comments and empty lines
            if ( line.strip() == "" ):
                continue
            if ( line.startswith == "#" ):
                continue
                
                
            ## Parse Line
            if ( is_tcp_log ):
                sample = TcpLogSample(line, timestamp, layout)
            else:
                sample = TcpProbeSample(line)

            ## generally exclude SSH
            if sample.is_ssh():
                continue


            ## To which connection does this line belong to?
            conn_key = sample.get_grouping_value()


            ## Sampling: drop line if timestamps are too close together
            if ( sampling ):
                if ( sample.reltime < last_sample[conn_key] + sampling ):
                    continue

                last_sample[conn_key] = sample.reltime



            ## adjust timestamp (if needed)
            sample.reltime += get_time_offset(sample)


            ## store values per connection
            connections[conn_key].append(sample)



    #print( connections.keys() )


    return connections



def get_common_base_time(connections, init=None):
    """
      Returns the minimal "abstime" from a number of connection, i.e. the "base_time"
    """

    ## use initialization value, if given
    if ( not init ):
        min_x = float("inf")
    else:
        min_x = init


    ## * find minimum of all connections *
    for key in connections:
        conn = connections[key]

        if ( conn[0].abstime < min_x ):
            min_x = conn[0].abstime

    return min_x



def get_common_base_time_from_files(files):
    ## NOTE: We have to decide if the file-open time from the header should be used as basis for the base_time, or the first sample...
    #    --> and be consistent with get_common_base_time(connections)

    min_x = float("inf")

    for filename in files:
        open_func = get_opener(filename)
        with open_func(filename, mode="tr", encoding="UTF-8") as f:

            timestamp, format = read_tcp_log_header( f )
            assert( format == "time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw" or
                    format == "time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw loss" )


            for line in f:
                ## skip comments and empty lines
                if ( line.strip() == "" ):
                    continue
                if ( line.startswith == "#" ):
                    continue

                ## Get first sample
                sample = TcpLogSample(line, timestamp)
                break

        if ( sample.abstime < min_x ):
            min_x = sample.abstime

    return min_x





def set_limits(args, ax):
    ax.set_xlim(xmin=args.x_min, xmax=args.x_max)
    ax.set_ylim(ymin=args.y_min, ymax=args.y_max)



def plot_cwnd_and_rtt(samples, args=None):
    X = [ x.reltime for x in samples ]
    Y1 = [ x.cwnd for x in samples ]
    Y2 = [ x.rtt for x in samples ]


    fig, ax_cwnd = plt.subplots()
    ax_rtt = ax_cwnd.twinx()

    ax_cwnd.set_xlabel('Time (s)')
    ax_cwnd.set_ylabel('CWnd (MSS)', color='blue')
    ax_rtt.set_ylabel('RTT (ms)', color='red')

    ax_cwnd.plot(X, Y1)
    ax_rtt.plot(X, Y2, color="red")

    ## axes limits, etc.
    if ( args ):
        set_limits(args, ax_cwnd)
        set_limits(args, ax_rtt)

    ## XXX experiment specific
    plot_min_rtt(ax_rtt)
    set_y_limits_1g(ax_cwnd, ax_rtt)

    #fig.legend()

    fig.canvas.get_default_filetype = lambda: "pdf"

    return fig


def plot_losses(samples, args=None):
    X = [ x.reltime for x in samples ]
    Y1 = [ x.cwnd for x in samples ]
    Y1a = [ x.ssthresh for x in samples ]
    Y2 = [ x.loss for x in samples ]


    fig, ax_cwnd = plt.subplots()
    ax_rtt = ax_cwnd.twinx()

    ax_cwnd.set_xlabel('Time (s)')
    ax_cwnd.set_ylabel('CWnd, SSThresh (MSS)', color='blue')
    ax_rtt.set_ylabel('Losses / Congestion', color='red')

    ax_cwnd.plot(X, Y1)
    ax_cwnd.plot(X, Y1a, color="green")
    ax_rtt.plot(X, Y2, color="red")

    adjust_y1_limits(ax_cwnd, Y1)
    adjust_y2_limits(ax_rtt, Y2)

    ## axes limits, etc.
    if ( args ):
        set_limits(args, ax_cwnd)
        set_limits(args, ax_rtt)

    #fig.legend()

    fig.canvas.get_default_filetype = lambda: "pdf"

    return fig




def plot_rtt(ax, samples, base_time=0.0, color=None, label=None):
    X = [ x.abstime - base_time for x in samples ]
    Y = [ x.rtt for x in samples ]


    ax.set_xlabel('Time (s)')
    ax.set_ylabel('RTT (ms)')


    ## * plot *
    if ( color ):
        ax.plot(X, Y, color=color, label=label)
        #ax.plot(X, Y, color=color, alpha=0.7)
    else:
        ax.plot(X, Y, label=label)


def plot_cwnd(ax, samples, base_time=0.0, color=None, label=None):
    X = [ x.abstime - base_time for x in samples ]
    Y = [ x.cwnd for x in samples ]


    ax.set_xlabel('Time (s)')
    ax.set_ylabel('CWnd (MSS)')


    ## * plot *
    if ( color ):
        ax.plot(X, Y, color=color, label=label)
        #ax.plot(X, Y, color=color, alpha=0.7)
    else:
        ax.plot(X, Y, label=label)





def plot_individually(args):
    """
      Major plotting mode:

      Plot all given files and all connections within these files in separate windows (CWnd and RTT).
    """


    ## read all files
    for filename in args.files:
        connections = read_file( filename, float(args.sampling)/1000 )

        ## plot all connections
        for key in connections:
            figure = plot_cwnd_and_rtt( connections[key], args )

            ## plot either each connection into a file
            out_filename = create_output_filename(filename, args.output, connections.keys(), key, args.output_dir)
            if ( args.output != "live" ):
                plt.savefig(out_filename, format=args.output)

                print(out_filename)
            ## or plot them "live"
            else:
                figure.canvas.set_window_title(out_filename)
                #figure.show()


            # XXX -- EXPERIMENTAL -- plot losses
            if ( args.output == "live" ):
                figure1 = plot_losses( connections[key], args )


def delete_aliens(conns):
    for k ,v in list(conns.items()):
        alien = True
        for x in v[:100]:
            if x.cwnd != 10:
                alien = False
                break
        if alien:
            del conns[k]




def plot_single_output(args):
    """
      Major plotting mode:

      Produce only one single output. Which data from which file should be plotted can/should be specified. (Not implemented, yet)
    """


    ## parse additional parameters

    # colors
    colors = args.colors
    if ( colors == None ):
        #colors = ["blue", "red"]
        colors = [None]

    legend = args.legend
    if ( not legend ):
        legend = [None]



    # which values to plot?
    rtt_enabled = False
    cwnd_enabled = False

    if ( args.rtt ):
        rtt_enabled = True
    if ( args.cwnd ):
        cwnd_enabled = True
    if ( args.rtt_and_cwnd ):
        rtt_enabled = True
        cwnd_enabled = True

    #   --> Note: If both switches are off, nothing would be plotted. Therefore, plot "everything" in this case.
    if ( not rtt_enabled and not cwnd_enabled ):
        rtt_enabled = True
        cwnd_enabled = True




    ## read all files
    connections = OrderedDict()

    for filename in args.files:
        conns = read_file( filename, float(args.sampling) )
        # append in deterministic order (for --filter and --color)
        for key in sorted(conns):
            connections[key] = conns[key]

    delete_aliens(connections)

    ## read reference files
    if ( args.reference_files ):
        base_time_ref_files = get_common_base_time_from_files(args.reference_files)
    else:
        base_time_ref_files = None


    ## get the minimal x-value from all connections and shift x-values by that amount
    common_base_time = get_common_base_time(connections, base_time_ref_files)


    ## create the single output axes; twin-x if rtt and cwnd will be plotted
    if ( rtt_enabled and cwnd_enabled ):
        fig, ax_cwnd = plt.subplots()
        ax_rtt = ax_cwnd.twinx()  ## twin-x
    elif ( rtt_enabled ):
        fig, ax_rtt = plt.subplots()
        ax_cwnd = None
    elif ( cwnd_enabled ):
        fig, ax_cwnd = plt.subplots()
        ax_rtt = None


    fig.canvas.get_default_filetype = lambda: "pdf"




    ## plot all connections
    plot_nr = 0
    for conn_nr, key in enumerate(connections,1 ):
        # filter
        if ( args.filter and conn_nr not in args.filter ):
            continue

        # * plot *
        if ( cwnd_enabled ):
            plot_cwnd( ax_cwnd, connections[key], common_base_time, colors[plot_nr % len(colors)], legend[plot_nr % len(legend)] )
            plot_nr += 1
        if ( rtt_enabled ):
            plot_rtt( ax_rtt, connections[key], common_base_time, colors[plot_nr % len(colors)], legend[plot_nr % len(legend)] )
            plot_nr += 1


    ## axes limits, etc.
    if ( cwnd_enabled ):
        set_limits(args, ax_cwnd)
        ax = ax_cwnd
    if ( rtt_enabled ):
        set_limits(args, ax_rtt)
        ax = ax_rtt
        

    if ( args.dashed_y_line ):
        plot_min_rtt(ax_rtt, args.dashed_y_line)


    ## XXX experiment specific
#    set_y_limits_10g(ax_cwnd, ax_rtt)

    if ( args.legend ):
        l = ax.legend(loc=args.legend_pos, ncol=args.legend_cols)

    ## save to file
    if ( args.output != "live" ):
        out_filename = get_output_filename(args)
        plt.savefig(out_filename, format=args.output)

        print(out_filename)



## MAIN ##
if __name__ == "__main__":

    ## Command line arguments
    import argparse

    parser = argparse.ArgumentParser()

    ## Logging
    #parser.add_argument("-p", "--port",
                        #help="Plot only connections on the given tcp-port.")

    parser.add_argument("-s", "--sampling", default=0,
                    help="Minimal time between two samples (in milliseconds)")

    parser.add_argument("-o", "--output", default = "live",
                        help="Set output type. Choices: live, pdf, Default: live")

    parser.add_argument("-d", "--output-dir")

    parser.add_argument("files", nargs="+",
                        help="Input File")

    parser.add_argument("-c", "--colors", nargs="*")

    parser.add_argument("-l", "--legend", nargs="*",
                        help="NOTE: Currently works only with --rtt and --color")

    parser.add_argument("-lp", "--legend-pos", type=int, default=0,
                    help="see: http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend")
                    
    parser.add_argument("-lc", "--legend-cols", type=int, default=1)

    parser.add_argument("-f", "--filter", nargs="*", type=int,
                        help="Plot only some connection, e.g. '--filter 1 3' for only the first and third connection. (Use together with '--one-output')" )

    parser.add_argument("-ref", "--reference-files", nargs='+', default=list(),
                        help="These files will be used to find a common base time, but will not be plotted. (Use together with '--one-output')")


    parser.add_argument("-x0", "--x-min", type=float)
    parser.add_argument("-x1", "--x-max", type=float)
    parser.add_argument("-y0", "--y-min", type=float)
    parser.add_argument("-y1", "--y-max", type=float)
    parser.add_argument("-y2", "--y2-min", type=float)
    parser.add_argument("-y3", "--y2-max", type=float)

    parser.add_argument("-yline", "--dashed-y-line", type=float)


    group = parser.add_mutually_exclusive_group()

    group.add_argument("-1", "--one-output", action="store_true",
                        help="Produce only one single output. Which data from which file should be plotted can/should be specified in additional parameters.")

    group.add_argument("-m", "--multi-output", action="store_true",
                        help="Plot all given files and all connections within these files in separate windows (CWnd and RTT).")


    #group = parser.add_mutually_exclusive_group()
    parser.add_argument("--rtt", action="store_true",
                       help="(Use together with '--one-output')")
    parser.add_argument("--cwnd", action="store_true",
                       help="(Use together with '--one-output')")
    parser.add_argument("--rtt_and_cwnd", action="store_true",
                       help="(Use together with '--one-output')")


    parser.add_argument("-of", "--output-filename",
                help="Sets the filename of output explicitly (helpful when '--one-output' is used).")



    args = parser.parse_args()



    ## Font sizes
    #rcParams = matplotlib.rcParams
    #rcParams["axes.labelsize"] = 24
    #rcParams["legend.fontsize"] = 20
    #rcParams["xtick.labelsize"] = 14
    #rcParams["ytick.labelsize"] = 14
    #rcParams.update({'figure.autolayout': True})

    # Workaround: "pdf-presenter-console" needs this, otherwise no text is displayed at all.
    matplotlib.rc('pdf', fonttype=42)


    # empty line
    if ( args.output != "live" ):
        print()


    if ( args.multi_output ):
        plot_individually(args)
    else:
        plot_single_output(args)



    if ( args.output == "live" ):
        #plt.title(filename)
        plt.show()



