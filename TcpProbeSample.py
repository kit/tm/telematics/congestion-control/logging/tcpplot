# -*- coding: utf-8 -*-

## Python 3

from ipaddress import ip_address

#  format of "/proc/net/tcpprobe"
#
#  11.30814 192.168.123.42:35120 193.99.144.87:80 1472 0x259a6f5 0x259a6f5 10 2147483647 19840 36604 150784
#  ^        ^                    ^                ^    ^         ^         ^  ^          ^     ^     ^
#  |        |                    |                |    |         |         |  |          |     |     +-------- [10] RWND
#  |        |                    |                |    |         |         |  |          |     +--------------  [9] sRTT (!) µs
#  |        |                    |                |    |         |         |  |          +--------------------  [8] Send window
#  |        |                    |                |    |         |         |  +-------------------------------  [7] Slow start threshold
#  |        |                    |                |    |         |         +----------------------------------  [6] Congestion window
#  |        |                    |                |    |         +--------------------------------------------  [5] Unacknowledged sequence #
#  |        |                    |                |    +------------------------------------------------------  [4] Next send sequence #
#  |        |                    |                +-----------------------------------------------------------  [3] Bytes in packet
#  |        |                    +----------------------------------------------------------------------------  [2] Receiver address:port
#  |        +-------------------------------------------------------------------------------------------------  [1] Sender address:port
#  +----------------------------------------------------------------------------------------------------------  [0] Time seconds
#TCP_PROBE_FORMAT = ['time', 'src', 'dst', 'packetSize', 'seq', 'unackSeq', 'cwnd', 'sst', 'swnd', 'rtt', 'rwnd']
#TCP_PROBE_INDICES = dict(zip(TCP_PROBE_FORMAT, range(len(TCP_PROBE_FORMAT))))



class TcpProbeSample:
    #TCP_PROBE_FORMAT = ['time', 'src', 'dst', 'packet_size', 'seq', 'unackSeq', 'cwnd', 'sst', 'swnd', 'rtt', 'rwnd']
    #TCP_PROBE_INDICES = dict(zip(TCP_PROBE_FORMAT, range(len(TCP_PROBE_FORMAT))))

    def __init__(self, line):
        input_list = line.strip().split(" ")
        #input_data = dict(zip(self.TCP_PROBE_FORMAT, input_list))

        ## Note: For better efficiency we could always work with input_list instead of input_data

        src_ip, src_separator, src_port = input_list[1].rpartition(':')
        dst_ip, dst_separator, dst_port = input_list[2].rpartition(':')
        assert src_separator
        assert dst_separator



        #self.timestamp = float( input_data['time'] )

        #self.src_ip = ip_address(src_ip.strip("[]"))
        #self.src_port = int( src_port )
        #self.src = str(src_ip) + ":" + str(src_port)

        #self.dst_ip = ip_address(dst_ip.strip("[]"))
        #self.dst_port = int( dst_port )
        #self.dst = str(dst_ip) + ":" + str(dst_port)

        #self.packet_size = int( input_data["packet_size"] )
        #self.sequence_number = int( input_data["seq"], 0 )
        #self.cwnd = int( input_data["cwnd"] )
        #self.ssthresh = int( input_data["sst"] )
        #self.rtt = float( input_data["rtt"] ) / 1000
        #self.rwnd = int( input_data["rwnd"] )


        self.reltime = float( input_list[0] )

        self.src_ip = ip_address(src_ip.strip("[]"))
        self.src_port = int( src_port )
        self.src = str(src_ip) + ":" + str(src_port)

        self.dst_ip = ip_address(dst_ip.strip("[]"))
        self.dst_port = int( dst_port )
        self.dst = str(dst_ip) + ":" + str(dst_port)

        self.packet_size = int( input_list[3] )
        self.sequence_number = int( input_list[4], 0 )
        self.cwnd = int( input_list[6] )
        self.ssthresh = int( input_list[7] )
        self.rtt = float( input_list[9] ) / 1000
        self.rwnd = int( input_list[10] )

        self.loss = None


    #def __getitem__(self, index):
        #if ( isinstance(index, str) ):
            #index = self.TCP_PROBE_INDICES[index]

            #return self.data[index]

    #def __setitem__(self, index, value):
        #if ( isinstance(index, str) ):
            #index = self.TCP_PROBE_INDICES[index]

            #self.data.__setitem__(self, index, value)



    def is_ssh(self):
        return self.src_port == 22 or self.dst_port == 22

    def to_list(self):
        return [ self.timestamp, self.src_ip, self.src_port, self.src, self.dst_ip, self.dst_port, self.dst,
                self.packet_size, self.sequence_number, self.cwnd, self.ssthresh, self.rtt, self.rwnd ]

    def get_grouping_value(self):
        return "%s:%s" % (self.dst_port, self.src_port)

    def __str__(self):
        return " ".join( (str(x) for x in self.to_list()) )




class TcpLogSample:

    def __init__(self, line, time_offset, layout = 1):
        ## #FORMAT: time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw [loss]

        input_list = line.strip().split(" ")

        input_list = dict(zip(layout, line.strip().split(' ')))

        self.rtt = float( input_list['rtt'] )

        if 'time' in input_list:
            # old format, also affects rtt field
            self.reltime = float( input_list['time'] )
            self.abstime = self.reltime + time_offset
            self.rtt /= 1000.0 # convert rtt to new format
        else:
            self.reltime = float(input_list['relativeTimestamp'])
            self.abstime = float(input_list['absoluteTimestamp'])

        self.src_ip = ip_address( input_list['srcIp'] )
        self.src_port = int( input_list['srcPort'] )

        self.dst_ip = ip_address( input_list['dstIp'] )
        self.dst_port = int( input_list['dstPort'] )

        self.cwnd = int( input_list['cwnd'] )

        if 'rwnd' in input_list:
            self.rwnd = int( input_list['rwnd'] )

        self.ssthresh = int( input_list['sst'] )

        if 'bw' in input_list:
            self.goodput = float( input_list['bw'] )
        elif 'throughput' in input_list:
            self.goodput = float( input_list['throughput'] ) if input_list['throughput'] != '_' else 0
            self.smoothed_goodput = float ( input_list['smoothedThroughput'] ) if input_list['smoothedThroughput'] != '_' else 0

        self.loss = float( input_list['loss'] ) if input_list.get('loss', '_') != '_' else 0

    def is_ssh(self):
        return self.src_port == 22 or self.dst_port == 22

    def to_list(self):
        return [ self.reltime, self.abstime, self.src_ip, self.src_port, self.dst_ip, self.dst_port,
                 self.cwnd, self.rwnd, self.ssthresh, self.rtt, self.loss ]

    def get_grouping_value(self):
        return "%s:%s" % (self.src_port, self.dst_port)

    def __str__(self):
        return " ".join( (str(x) for x in self.to_list()) )





